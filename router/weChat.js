const express = require("express");
const router = express.Router(); // 配置路由模块
const validateToken = require("../util/validateToken");
const { setTonken, timingSetTonken } = require("../util/tonkenConfig");
const formmessage = require("./formmessage");

// // 项目启动后获取Tonken
// setTonken().then(() => {
//   timingSetTonken();
// });


// get请求验证tonken有效性
router.get("/", (req, res) => {
  validateToken(req).then((t) => {
    res.send(t);
  });
});

// 接收到微信请求
router.post("/", (req, res) => {
  // 调用方法回复微信消息
  formmessage(req.body).then((xml) => {
    res.send(xml);
  });
});

module.exports = router;
